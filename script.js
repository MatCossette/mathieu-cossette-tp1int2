const slides = document.querySelector('.slides');
const image = document.querySelectorAll('.slides span');

const prev = document.querySelector('.prev');
const next = document.querySelector('.next');

let counter = 0;
const size = (image[0].clientWidth)/16;

next.addEventListener('click', function() {
    if (counter >= 1){
        return;
    }
    slides.style.transition = "transform 1s ease-in-out";
    counter++;
    slides.style.transform = 'translateX(' + (-size * counter) +'rem)';
});

prev.addEventListener('click', function() {
    if (counter <= 0){
        return;
    }
    slides.style.transition = "transform 1s ease-in-out";
    counter--;
    slides.style.transform = 'translateX(' + (-size * counter) +'rem)';
});

